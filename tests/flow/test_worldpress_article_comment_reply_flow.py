import time
from base64 import b64encode
import requests
import pytest
import lorem

editor_username = 'editor'
editor_name = 'John Editor'
editor_password = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
editor_token = b64encode(f"{editor_username}:{editor_password}".encode('utf-8')).decode("ascii")

commenter_username = 'commenter'
commenter_name = 'Albert Commenter'
commenter_password = 'SXlx hpon SR7k issV W2in zdTb'
commenter_token = b64encode(f"{commenter_username}:{commenter_password}".encode('utf-8')).decode("ascii")

blog_url = 'https://gaworski.net'
posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"
comments_endpoint_url = blog_url + "/wp-json/wp/v2/comments"
users_endpoint_url = blog_url + "/wp-json/wp/v2/users"


@pytest.fixture(scope='module')
def time_stamp():
    return str(int(time.time()))


@pytest.fixture(scope='module')
def editor_header():
    return {
        "Content-Type": "application/json",
        "Authorization": "Basic " + editor_token
    }


@pytest.fixture(scope='module')
def commenter_header():
    return {
        "Content-Type": "application/json",
        "Authorization": "Basic " + commenter_token
    }


@pytest.fixture(scope='module')
def article_content(time_stamp):
    return {
        "title": "FLOW Test Karol Orzech " + time_stamp,
        "excerpt": lorem.sentence(),
        "content": lorem.paragraph(),
        "status": "publish"
    }


@pytest.fixture(scope='module')
def comment_content(time_stamp):
    return {
        "content": "My comment Karol Orzech " + time_stamp,
        "post": None
    }


@pytest.fixture(scope='module')
def reply_content(time_stamp):
    return {
        "content": "My reply Karol Orzech " + time_stamp,
        "post": None,
        "parent": None
    }


def test_article_comment_reply_flow(
        editor_header,
        commenter_header,
        article_content,
        comment_content,
        reply_content):

    # ARTICLE creation:
    article_create_response = requests.post(url=posts_endpoint_url, headers=editor_header, json=article_content)
    assert article_create_response.status_code == 201

    # Extracting ARTICLE_id and connecting it with COMMENT:
    article_id = article_create_response.json()['id']
    comment_content['post'] = article_id

    # Getting response from ARTICLE - testing status and creator:
    article_endpoint_url = posts_endpoint_url + "/" + str(article_id)
    article_content_response = requests.get(url=article_endpoint_url)
    assert article_content_response.status_code == 200
    article_content_json = article_content_response.json()
    assert article_content_json["status"] == 'publish'
    assert article_content_json["author"] == 2

    # COMMENT creation:
    comment_create_response = requests.post(url=comments_endpoint_url, headers=commenter_header, json=comment_content)
    assert comment_create_response.status_code == 201

    # Extracting COMMENT_id and connecting it and ARTICLE_id with REPLY:
    comment_id = comment_create_response.json()["id"]
    reply_content['post'] = article_id
    reply_content['parent'] = comment_id

    # Getting response from COMMENT - testing status, creator and connection with ARTICLE:
    comment_endpoint_url = comments_endpoint_url + "/" + str(comment_id)
    comment_content_response = requests.get(url=comment_endpoint_url)
    assert comment_content_response.status_code == 200
    comment_content_json = comment_content_response.json()
    assert comment_content_json["status"] == 'approved'
    assert comment_content_json["post"] == article_id
    assert comment_content_json["author_name"] == commenter_name

    # REPLY creation:
    reply_create_response = requests.post(url=comments_endpoint_url, headers=editor_header, json=reply_content)
    assert reply_create_response.status_code == 201

    # Extracting REPLY_id:
    reply_id = reply_create_response.json()['id']

    # Getting response from REPLY - testing status, creator and connection with ARTICLE and COMMENT:
    reply_endpoint_url = comments_endpoint_url + "/" + str(reply_id)
    reply_content_response = requests.get(url=reply_endpoint_url)
    assert reply_content_response.status_code == 200
    reply_content_json = reply_content_response.json()
    assert reply_content_json["status"] == 'approved'
    assert reply_content_json["parent"] == comment_id
    assert reply_content_json["post"] == article_id
    assert reply_content_json["author_name"] == editor_name
