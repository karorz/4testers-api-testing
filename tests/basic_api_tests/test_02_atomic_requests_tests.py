import requests


def test_status_code():
    response = requests.get("https://api.ipify.org/?format=json")
    assert response.status_code == 200


def test_response_has_a_body():
    response = requests.get("https://api.ipify.org/?format=json")
    data = response.json()
    assert data is not None


def test_is_json():
    response = requests.get("https://api.ipify.org/?format=json")
    assert response.headers["Content-type"] == "application/json"
